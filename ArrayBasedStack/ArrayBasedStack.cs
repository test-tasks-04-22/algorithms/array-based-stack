﻿namespace ArrayBasedStack;

public class Stack<T>
{
    private T[] _items;
    private int _count;
    private const int N = 10;
    public Stack()
    {
        _items = new T[N];
    }
    public Stack(int length)
    {
        _items = new T[length];
    }

    private bool IsEmpty => _count == 0;

    public void Push(T item)
    {
        if (_count == _items.Length)
            Resize(_items.Length + 10);
 
        _items[_count++] = item;
    }
    public T Pop()
    {
        if (IsEmpty)
            throw new InvalidOperationException("Стек пуст");
        var item = _items[--_count];
        _items[_count] = default(T) ?? throw new InvalidOperationException();
 
        if (_count > 0 && _count < _items.Length - 10)
            Resize(_items.Length - 10);
 
        return item;
    }
    public T Peek()
    {
        return _items[_count - 1];
    }
 
    private void Resize(int max)
    {
        var tempItems = new T[max];
        for (var i = 0; i < _count; i++)
            tempItems[i] = _items[i];
        _items = tempItems;
    }
}
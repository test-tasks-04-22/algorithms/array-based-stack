﻿var stack = new ArrayBasedStack.Stack<string>(3);
stack.Push("1");
stack.Push("2");
stack.Push("3");
stack.Push("4");

var head = stack.Pop();
Console.WriteLine($"Извлечённый элемент: {head}"); 

head = stack.Peek();
Console.WriteLine($"Последний элемент списка: {head}");  
